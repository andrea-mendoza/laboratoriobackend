package com.sisweb.labo2.Services;

import com.sisweb.labo2.Entities.Genre;
import com.sisweb.labo2.Repository.GenreRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

@Service
public class GenreServiceImpl implements GenreService {

    @Autowired
    private GenreRepository genreRepository;

    @Autowired
    @Qualifier(value="genreRepository")
    public  void setGenreRepository(GenreRepository genreRepository){
        this.genreRepository = genreRepository;
    }

    @Override
    public Iterable<Genre> listAllGenre() {
        return genreRepository.findAll();
    }
}
