package com.sisweb.labo2.Services;

import com.sisweb.labo2.Entities.Genre;

public interface GenreService {
    Iterable<Genre> listAllGenre();
}
