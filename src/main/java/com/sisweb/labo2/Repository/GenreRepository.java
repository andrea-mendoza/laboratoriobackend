package com.sisweb.labo2.Repository;

import com.sisweb.labo2.Entities.Genre;
import org.springframework.data.repository.CrudRepository;

import javax.transaction.Transactional;

@Transactional
public interface GenreRepository extends CrudRepository<Genre, Integer>{
}
