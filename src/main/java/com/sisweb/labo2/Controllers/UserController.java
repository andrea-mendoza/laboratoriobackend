package com.sisweb.labo2.Controllers;

import com.sisweb.labo2.Entities.User;
import com.sisweb.labo2.Services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import javax.validation.Valid;
import java.util.List;

@Controller
@CrossOrigin(origins = "http://localhost:4200")
public class UserController {

    @Autowired
    private UserService userService;

    @RequestMapping("/")
    public String welcome() {
        return "home";
    }

    @PostMapping(value="/newUser",consumes = "application/json")
    @ResponseStatus(HttpStatus.CREATED)
    public void createUser( @Valid @RequestBody User user ) {
        userService.saveUser(user);
    }

    @DeleteMapping(value="/deleteUser/{id}")
    @ResponseStatus(HttpStatus.OK)
    public @ResponseBody void deleteUser(@PathVariable Integer id) {
        userService.deleteUser(id);
    }

    @GetMapping(value="/allUsers")
    @ResponseStatus(HttpStatus.OK)
    public  @ResponseBody List<User> getAllUsers() {
        List<User> users = (List<User>) userService.listAllUsers();
         return users;
    }

    @GetMapping(value="/showUser/{id}",produces = "application/json")
    @ResponseStatus(HttpStatus.OK)
    public @ResponseBody User getOneUser( @PathVariable Integer id) {
        return userService.getUser(id);
    }

    @GetMapping(value="/showUserByName/{name}",produces = "application/json")
    @ResponseStatus(HttpStatus.OK)
    public @ResponseBody  List<User> getUserByName(@PathVariable String name) {
        List<User> users = (List<User>) userService.getUserByName(name);
        return users;
    }

    @PutMapping(value = "/editUser/{id}")
    @ResponseStatus(HttpStatus.OK)
    public @ResponseBody User editUser(@PathVariable Integer id, @Valid @RequestBody User user ) {
        User us = userService.getUser(id);

        userService.saveUser(us);
        return userService.getUser(id);
    }

    @PostMapping(value="/login",consumes = "application/json")
    @ResponseStatus(HttpStatus.OK)
    public @ResponseBody boolean loginUser( @Valid @RequestBody User user ) {
        User userLogin = userService.getUserByemail(user.getEmail());
        String password =  userLogin.getPassword();
        if(userLogin == null)
        {
            return false;
        }
        if(user.getPassword().equals(password)) {
            return true;
        }
        else{
            return false;
        }
    }
}
