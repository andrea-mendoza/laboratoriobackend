package com.sisweb.labo2.Controllers;

import com.sisweb.labo2.Entities.Genre;
import com.sisweb.labo2.Services.GenreService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

import java.util.List;

@Controller
@CrossOrigin(origins = "http://localhost:4200")
public class GenreController {

    @Autowired
    private GenreService genreService;

    @GetMapping(value="/allGenres")
    @ResponseStatus(HttpStatus.OK)
    public  @ResponseBody
    List<Genre> getAllGenres() {
        List<Genre> genres = (List<Genre>) genreService.listAllGenre();
        return genres;
    }
}
