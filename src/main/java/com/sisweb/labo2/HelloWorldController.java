package com.sisweb.labo2;

import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
class HelloWorldController {

//    @RequestMapping(value="/",produces = "MediaType.TEXT_HTML_VALUE")
//    String home() {
//        return "home";
//    }

    @RequestMapping("/listar")
    String listar() {
        return "Aca voy a mostrar un listado";
    }
}